package homebrew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomebrewRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomebrewRestApplication.class, args);
    }

}
